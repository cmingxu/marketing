[Flow diagram](https://gitlab.com/gitlab-org/marketing_monthly_release/issues/1#note_3984150)

## Redirect sign up

You should receive an email shortly to confirm your email address. 

If you don't see an email in 24 hours, please check your spam folder. Contact us at community@gitlab.com if you don't receive an email.

(Quick Start Links)

## Email Confirmation

if: Signed up to GitLab.com
From: Community@gitlab.com

Thanks for signing up! Before you can get started, you must follow the link below to activate your account.

Welcome to GitLab! If you have any questions, please contact us. 

The GitLab Team
https://about.gitlab.com


## Email Welcome to GitLab.com

- If: Signed up to GitLab.com

Subject: Welcome to GitLab.com!

Quick start: Get your project set up on GitLab.com

Button: Get started

Join us: Join our next live GitLab Intro webcast.

Button: Sign up

## Email Welcome - EE Trial

- Downloaded EE Trial

Subject: Welcome to GitLab.com!

GitLab EE

Install guide: Get started with GitLab EE on-premises.

Button: Install

Quick start guide: How to get the most out of managing your projects in GitLab.

Button: Quick start

Join us: Join our next live GitLab Intro webcast.

Button: Sign up



## X days later

(Business Dev)

## Start your EE free trial

- Signed up for webcast - indicated they are interested in on-permises
- No EE trial yet


